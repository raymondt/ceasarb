/*
 * =====================================================================================
 *
 *       Filename:  caesarb.c
 *
 *    Description:  A simple Caesar cypher program using a block key. 
 *
 *        Version:  1.0
 *        Created:  10/08/19
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Raymond Thomson
 *		  Contact:	raymond.thomson76@gmail.com
 *      Copyright:  Copyright (c) 2019, Raymond Thomson
 *		  License:	GNU General Public License
 *
 * =====================================================================================
 */

#define a (int)'a'
#define z (int)'z'
#define A (int)'A'
#define Z (int)'Z'

#include <stdio.h>
#include <unistd.h>
#include <string.h>

int getnextchar(char ins[256]);

int main (int ARGC,char *ARGV[]) {

	int slen = 0;

	// check program arguments
	if (ARGC == 1 || ARGC == 2) {
		printf("caesar needs a key to encrypt the input.\ncaesar (-d or -e) 'KEY' < STDIN > STDOUT\n");
		return 1;
	}
	if (ARGC > 3) {
		printf("too many arguments for caesar.\ncaesar (-d or -e)'KEY' < STDIN > STDOUT\n");
		return 2;
	}
	slen = strlen(ARGV[2]);
	if (slen > 255) {
		printf("max length of key = 255 characters.\n");
		return 3;
	}

	// load key
	if (getnextchar(ARGV[2]) == -1) return 4;
	
	char ch; 			// current buffer character
	char kch; 			// current key character
	char buf[BUFSIZ];	// STDIN buffer
	int bc;				// current buffer read count
	int opt = 0;		// options for encrypt or decrypt

	// set and check encrypt or decrypt option
	if (strstr(ARGV[1],"-e") != NULL) opt = 1;
	if (strstr(ARGV[1],"-d") != NULL) opt = 2;
	if (opt == 0) {	
		printf("you must add -d or -e to decrypt or encrypt.\ncaesar (-d or -e) 'KEY' < STDIN > STDOUT\n");
		return 5;
	}

	while ((bc = read(0, buf, BUFSIZ)) > 0) {
		for (int i = 0;i < bc;i++) {
			ch = buf[i];
			kch = getnextchar(NULL);
			if (opt == 1) {
				if (ch >= a && ch <= z) {
					buf[i] = (((ch-a)+kch)%26)+a;
				} else if (ch >= A && ch <= Z) {
					buf[i] = (((ch-A)+kch)%26)+A;
				}
			}
			if (opt == 2) {
				if (ch >= a && ch <= z) {
					buf[i] = (((ch-a)-kch)%26)+a;
				} else if (ch >= A && ch <= Z) {
					buf[i] = (((ch-A)-kch)%26)+A;
				}
			}
		}
		write(1, buf, bc);
	}

	return 0;
}

int getnextchar(char ins[256]) {
	static int cloc;
	static int clen;
	static char cbuf[256];

	if (ins != NULL) {
		cloc = 0;
		clen = strlen(ins);
		for (int i = 0; i < clen; i++) {
			if (ins[i] >= a && ins[i] <= z){ 
				cbuf[i] = (int) ins[i] - a;
			} else if (ins[i] >= A && ins[i] <= Z) {
				cbuf[i] = (int) ins[i] - A;
			} else {
				printf ( "The key must only contain letters 'a' to 'z' and 'A' to 'Z'.\n" );
				return -1;
			}
		}
		return clen;	
	}

	if (ins == NULL) {
		int tc = cbuf[cloc];
		cloc++;
		if (cloc >= clen) cloc = 0;
		return tc;
	}

return -1;
}
