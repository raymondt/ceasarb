# Application : caesarb - caesar block cypher cli program
#	Description
# Author Raymond Thomson

all: help

CC=gcc
MAN=man/
SRC=src/
CFLAGS=-I.
LIBS=-lm

%.o: %.c
	@echo 'Invoking: GCC C object files'
	$(CC) -c -o $($SRC)$@ $< $(CFLAGS)

caesarb: $(SRC)caesarb.o
	@echo 'Building and linking target: $@'
	$(CC) -o caesarb $(SRC)*.o $(LIBS)

local: caesarb clean

clean:
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	@echo 'Installed. Enter ./caesarb to run'

install: caesarb 
	@echo 'Installing'	
	cp caesarb /usr/local/bin/caesarb
	cp $(MAN)caesarb /usr/share/man/man1/caesarb.1
	gzip /usr/share/man/man1/caesarb.1
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	rm -f caesarb
	@echo 'installed, type caesarb to run or man caesarb for the manual'

remove:
	rm -f caesarb

uninstall:
	rm -f /usr/local/bin/caesarb
	rm -f /usr/share/man/man1/caesarb.1.gz
	@echo 'caesarb uninstalled.'

help:
	@echo 'Make options for caesarb'
	@echo 'Local Folder make'
	@echo '    make local'
	@echo 'Local Folder uninstall'
	@echo '    make remove'
	@echo 'System Install'
	@echo '    sudo make install'
	@echo 'System Uninstall'
	@echo '    sudo make uninstall'
	
