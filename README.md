# caesarb

This program is developed to perform a block caesar cypher on STDIN, and output the results to STDOUT.

Useful for performing the cypher on ASCII letters 'a' to 'z' & 'A' to 'Z'.

-e for encryption
-d for decryption

the key = array of characters upto 255 in length. 

This program can be installed by downloading and extracting the source files. Opening a terminal in the new directory and typing "sudo make install" or "make" to see the installation options.
